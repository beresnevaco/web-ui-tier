import axios from 'axios';
import { endpointUrls } from './config';

export const webApi = {
    getListOfInstruments: () => {
        return axios.get(endpointUrls.webApiUrl + '/api/instrument/list', {
            headers: {
                'Authorization': localStorage.getItem('user_jwt')
            }
        });
    },
    getListOfCpts: () => {
        return axios.get(endpointUrls.webApiUrl + '/api/cpt/list', {
            headers: {
                'Authorization': localStorage.getItem('user_jwt')
            }
        });
    },
    getHistoricalData: (instrument, fromDateTime, toDateTime) => {
        return axios.get(endpointUrls.webApiUrl + '/api/instrument/' + instrument + '/' + fromDateTime + '/' + toDateTime, {
            headers: {
                'Authorization': localStorage.getItem('user_jwt')
            }
        });
    },
    getRealTimeDataStream: (instrument) => {
        return new EventSource(endpointUrls.webApiUrl + '/api/instrument/' + instrument + '/realtime');
    },
    getCptStat: (fromDateTime, toDateTime) => {
        return axios.get(endpointUrls.webApiUrl + '/api/counterparty/stats/' + fromDateTime + '/' + toDateTime, {
            headers: {
                'Authorization': localStorage.getItem('user_jwt')
            }
        });
    },
    login: (login, password) => {
        return axios.post(endpointUrls.webApiUrl + '/api/login', {
            "user_login": login,
            "user_password": password
        });
    }
}