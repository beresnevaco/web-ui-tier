import React, { useState } from 'react';
import Container from './components/Container';
import NavBar from './components/NavBar';


class App extends React.Component {
  render() {
    return (
      <div>
        <NavBar/>
        <Container/>
      </div>
    );  
  }
}

export default App;
