import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import TradersView from '../views/TradersView';
import InstrumentsView from '../views/InstrumentsView';
import LoginViwe from '../views/LoginView';
import { PrivateRoute } from '../PrivateRoute';


class Container extends Component {
  render() {
    return (
      <Switch>
        <PrivateRoute path='/cpts' component={TradersView} />
        <PrivateRoute path='/instruments' component={InstrumentsView} />
        <PrivateRoute path='/logout' component={LoginViwe}/>
        <Route path='/login' component={LoginViwe} />
      </Switch>
    );
  }
}

export default Container;