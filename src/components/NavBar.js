import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

class NavBar extends Component {
    constructor() {
        super();
    }

    isLoggedIn = () => {
        return localStorage.getItem('user_authorized') == 'true';
    }

    getUsername = () => {
        return localStorage.getItem('user_login')
    }

    handleLogout = () => {
        console.log('handleLogout');
        localStorage.setItem('user_authorized', false);
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="#">Market Data v1.0</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <NavLink className="nav-link" to='/instruments'>Instruments</NavLink>
                        </li>
                        <li>
                            <NavLink className="nav-link" to='/cpts'>Counterparties</NavLink>
                        </li>
                    </ul>
                </div>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <NavLink className="nav-link link" to='/login'>Logout</NavLink>
                    </li>
                </ul>
            </nav>
        );
    }
}

export default NavBar;