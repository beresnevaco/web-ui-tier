import React from 'react';
import { Line, Chart } from 'react-chartjs-2';

const initialState = {
    labels: Array.from(Array(30).keys()),
    datasets: [
        {
            label: 'sell',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(55,192,55,0.4)',
            borderColor: 'rgba(55,192,55,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(55,192,55,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
        },
        {
            label: 'buy',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(192,55,55,0.4)',
            borderColor: 'rgba(192,55,55,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(192,55,55,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(192,55,55,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
        }
    ]
};

class LineChart extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState(initialState)
    }

    componentDidMount() {
        setInterval(() => {
            var oldDataSet = this.state.datasets[0];
            var oldDataSet2 = this.state.datasets[1];
            var newData = []
            var newData2 = []

            this.props.marketData.sell.forEach(m => newData.push(m));
            this.props.marketData.buy.forEach(m => newData2.push(m));

            var newDataSet = {
                ...oldDataSet
            };

            var newDataSet2 = {
                ...oldDataSet2
            };

            newDataSet.data = newData;
            newDataSet2.data = newData2;

            var newState = {
                ...initialState,
                datasets: [newDataSet, newDataSet2]
            };

            

            this.setState(newState);
        }, this.props.updateFrequencyMs);
    }

    render() {
        return (
            <Line data={this.state} />
        );
    }
};

export default LineChart;