import React, { Component } from 'react';

class Statistics extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card text-white bg-dark mb-3 h-100">
                <div className="card-body align-top">
                <h4 className="card-title">Statistics</h4>
                <hr className='bg-white'/>
                <p>
                    Avg Sell Price: <span>{this.props.avgSellPrice()}</span>
                </p>
                <p>
                    Avg Buy Price: <span>{this.props.avgBuyPrice()}</span>
                </p>
                </div>
            </div>
        );
    }
}

export default Statistics;
