import React, { Component } from 'react';
import { webApi } from '../webService'

class TradersView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cpts: []
    }
  }

  getCptsStat = () => {
    let now = Math.round(new Date().getTime() / 1000);
    let from = now - 100000;
    webApi.getCptStat(from, now)
      .then(ret => {
        let list = [];
        ret.data.forEach(cpty => list.push(cpty));
        this.setState({
          cpts: list
        })
      })
      .catch(e => {
        console.log('Unable to receive cpty stat')
      })
  }

  doRefresh = () => {
    this.getCptsStat();
  }

  componentWillMount() {
    this.getCptsStat();
  }

  render() {
    return (
      <div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Cpt</th>
              <th scope="col">Realized profit</th>
              <th scope="col">Unrealized profit</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.cpts
                .map(item => (
                  <tr>
                    <td>{item.counter_party_name}</td>
                    <td>{item.realized_profit}</td>
                    <td>{item.effective_profit}</td>
                  </tr>
                ))
            }
          </tbody>
        </table>
        <button
          className='btn btn-dark float-right mr-5'
          onClick={this.doRefresh}
        >
          Refresh
          </button>
      </div>
    );
  }
}

export default TradersView;