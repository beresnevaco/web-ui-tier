import React, { Component } from 'react';
import { webApi } from '../webService'

class LoginViwe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    }
  }

  componentDidMount() {
      localStorage.clear();
  }

  handleLogin = (e) => {
    webApi.login(this.state.username, this.state.password)
      .then(ret => {
        localStorage.setItem('user_login', this.state.username);
        localStorage.setItem('user_jwt', ret.data.auth_token);
        localStorage.setItem('user_authorized', true)
        this.props.history.push("/instruments");
      })
      .catch(e => {
        console.log('Unable to log in');
        alert('Unable to login');
      });
  }

  handleUsernameChanged = (e) => {
    this.setState({
      username: e.target.value
    });
  }

  handlePasswordChanged = (e) => {
    this.setState({
      password: e.target.value
    });
  }

  render() {
    return (
      <div class="container-fluid h-100 w-100 mt-5">
        <div class="row justify-content-center">
          <div class="card-wrapper w-50">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Login</h4>
                <div class="my-login-validation" novalidate="">

                  <div class="form-group">
                    <label for="email">Username</label>
                    <input
                      id="email"
                      type="email"
                      class="form-control"
                      name="email"
                      onChange={this.handleUsernameChanged}
                      value={this.state.username}
                      required autofocus
                    />
                  </div>

                  <div class="form-group">
                    <label for="password">Password
                  </label>
                    <input
                      id="password"
                      type="password"
                      class="form-control"
                      name="password"
                      onChange={this.handlePasswordChanged}
                      value={this.state.password}
                      required data-eye
                    />
                  </div>
                  <div class="form-group m-0">
                    <button
                      className="btn btn-primary btn-block"
                      onClick={this.handleLogin}
                    >
                      Login
                     </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginViwe;