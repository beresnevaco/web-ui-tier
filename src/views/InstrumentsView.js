import React, { Component } from 'react';
import LineChart from '../components/LineChart'
import Statistics from '../components/Statistics'
import { webApi } from '../webService'

class InstrumentsView extends Component {
    constructor() {
        super();
        this.state = {
            instrumentNames: ['Choose instrument'],
            periods: ['Real-Time', '5 min', '15 min', '30 min', '1 h', '1 d'],
            selectedInstrument: 'Choose instrument',
            selectedPeriod: 'Real-Time',
            marketData: {
                sell: [],
                buy: []
            },
            streamedData: {
                sell: [],
                buy: []
            },
            historicalData: {
                sell: [],
                buy: []
            },
            sse: null
        }
    }

    avgSellPrice = () => {
        return this.avg(this.state.marketData.sell);
    }

    avgBuyPrice = () => {
        return this.avg(this.state.marketData.buy);
    }

    avg = (list) => {
        let val = list.reduce((p, c) => p + c, 0) / list.length;
        return Math.round(val * 100) / 100
    }

    handleChangeSelectedPeriod = (e) => {
        this.setState({
            selectedPeriod: e.target.value
        });
        this.getData(e.target.value, this.state.selectedInstrument);
    }

    handleChangeSelectedInstrument = (e) => {
        this.setState({
            selectedInstrument: e.target.value
        });
        this.getData(this.state.selectedPeriod, e.target.value)
    }

    getData = (period, instrument) => {
        if (period === 'Real-Time') {
            this.setState({
                marketData: this.getStreamedDataForSelectedInstrument(instrument)
            });
        } else {
            let MS_PER_MINUTE = 60000;
            let MIN_PER_HOUR = 60;
            let to = new Date().getTime();
            let from = to;

            if (period === '5 min') {
                from = to - 5 * MS_PER_MINUTE;
            } else if (period === '15 min') {
                from = to - 15 * MS_PER_MINUTE;
            } else if (period === '30 min') {
                from = to - 30 * MS_PER_MINUTE;
            } else if (period === '1 h') {
                from = to - MIN_PER_HOUR * MS_PER_MINUTE;
            } else if (period === '1 d') {
                from = to - 24 * MIN_PER_HOUR * MS_PER_MINUTE;
            }

            this.setState({
                marketData: this.getHistoricalDataForSelectedInstrument(instrument, from, to)
            });
        }
    }

    getInstrumentNames = () => {
        let instrumentNames = this.state.instrumentNames;
        webApi.getListOfInstruments()
            .then((ret) => {
                ret.data.forEach(e => instrumentNames.push(e.instrument_name));
                this.setState({
                    instrumentNames: instrumentNames
                });
            })
            .catch((e) => {
                console.log('Unable to receive instruments');
            })
    }

    getStreamedDataForSelectedInstrument = (instrument) => {
        if (this.state.sse !== null) {
            this.state.sse.close();
        }
        
        let sse = webApi.getRealTimeDataStream(instrument);
        sse.onmessage = (m) => {
            console.log(m)
            this.processMeesage(JSON.parse(m.data), this.state.streamedData)
            this.setState({
                marketData: this.state.marketData
            })
        }

        this.setState({
            sse: sse
        });
        return this.state.streamedData;
    }

    getHistoricalDataForSelectedInstrument = (instrument, fromDateTime, toDateTime) => {
        webApi.getHistoricalData(instrument, fromDateTime, toDateTime)
            .then(ret => {
                let historicalData = this.state.historicalData;
                historicalData.splice(0, historicalData.length);
                ret.data.forEach(m => {
                    this.processMeesage(m, historicalData)
                });
            })
            .catch(e => {
                console.log('Unable to load historical data');
            })
        return this.state.historicalData;
    }

    processMeesage = (msg, dataSource) => {

        console.log(msg.type);
        if (msg.type == "S") {
            console.log('inside S')
            if (dataSource.sell.length >= 30) {
                dataSource.sell.splice(0, 1);
            }
            dataSource.sell.push(Math.round(msg.price));
        }

        if (msg.type == "B") {
            console.log('inside B')
            if (dataSource.buy.length >= 30) {
                dataSource.buy.splice(0, 1);
            }
            dataSource.buy.push(Math.round(msg.price));
        }

        console.log(msg)

    }

    componentWillMount() {
        this.getInstrumentNames();
    }

    render() {
        return (
            <div className='container-fluid'>
                <div className="row pt-3">
                    <div className='col'>
                        <div className="form-row">
                            <div className="col-7">
                                <div class="form-group">
                                    <label for="intrument">Trading instrument</label>
                                    <select
                                        id='intrument'
                                        className='custom-select'
                                        onChange={this.handleChangeSelectedInstrument}
                                        value={this.state.selectedInstrument}
                                    >
                                        {
                                            this.state.instrumentNames
                                                .map(key => (
                                                    <option key={key} value={key}>
                                                        {key}
                                                    </option>
                                                ))
                                        }
                                    </select>
                                </div>

                            </div>
                            <div className="col">
                                <div class="form-group">
                                    <label for="timePeriod">Time period</label>
                                    <select
                                        id='timePeriod'
                                        className='custom-select'
                                        onChange={this.handleChangeSelectedPeriod}
                                        value={this.state.sl}

                                    >
                                        {
                                            this.state.periods
                                                .map(key => (
                                                    <option key={key} value={key}>
                                                        {key}
                                                    </option>
                                                ))
                                        }
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div className="row pt-3">
                    <div className='col-9'>
                        <LineChart
                            updateFrequencyMs={300}
                            marketData={this.state.marketData}
                        />
                    </div>
                    <div className='col-3 stat-board'>
                        <Statistics
                            avgSellPrice={this.avgSellPrice}
                            avgBuyPrice={this.avgBuyPrice}
                        />
                    </div>
                </div>
            </div>
        );
    }

}

export default InstrumentsView;